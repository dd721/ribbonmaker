﻿using System;
using System.Drawing;

namespace RibbonMaker.Draw
{
    public class BendThirty : RibbonElement
    {
        public BendThirty() : base("Diagonal (30 Degrees)") { }

        public override RibbonElement GetInstance()
        {
            return new BendThirty();
        }
        public override Image Draw(bool texture)
        {
            Clear();
            if (XMirror ^ YMirror)
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 7 - (width / 2) + (WIDTH / 2) + (Pos / 2) + (i / 2))), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 7 - (width / 2) + (WIDTH / 2) - (Pos / 2) + (i / 2))), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 10 + Pos + (i / 2))), i, color);
                    }
                }
            }
            else
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 7 - (width / 2) + (WIDTH / 2) + (Pos / 2) - (i / 2))), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 7 - (width / 2) + (WIDTH / 2) - (Pos / 2) - (i / 2))), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 5 + Pos - (i / 2))), i, color);
                    }
                }
            }
            if (texture) Texture();
            return Bmp;
        }
    }
}