﻿using System;
using System.Drawing;

namespace RibbonMaker.Draw
{
    public class BendFifteen : RibbonElement
    {
        public BendFifteen() : base("Diagonal (15 Degrees)") { }

        public override RibbonElement GetInstance()
        {
            return new BendFifteen();
        }
        public override Image Draw(bool texture)
        {
            Clear();
            if (XMirror ^ YMirror)
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 5 - (width / 2) + (WIDTH / 2) + (Pos / 2) + (i / 3))), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 5 - (width / 2) + (WIDTH / 2) - (Pos / 2) + (i / 3))), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 7 + Pos + (i / 3))), i, color);
                    }
                }
            }
            else
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 5 - (width / 2) + (WIDTH / 2) + (Pos / 2) - (i / 3))), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 5 - (width / 2) + (WIDTH / 2) - (Pos / 2) - (i / 3))), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 2 + Pos - (i / 3))), i, color);
                    }
                }
            }
            if (texture) Texture();
            return Bmp;
        }
    }
}