﻿using System;
using System.Drawing;

namespace RibbonMaker.Draw
{
    public class HorizontalBar : RibbonElement
    {
        public HorizontalBar() : base("Horizontal Bar") { }
        public int Pos
        {
            get
            {
                if (!Center) return pos;
                else return HEIGHT / 2;
            }
            set { pos = value; }
        }
        public override RibbonElement GetInstance()
        {
            return new HorizontalBar();
        }
        public override Image Draw(bool texture)
        {
            Clear();
            if (symmetry)
            {
                FillRectangle(
                    Bmp,
                    color,
                    0,
                    (HEIGHT / 2) - (Pos / 2) - (width / 2),
                    WIDTH,
                    (HEIGHT / 2) - (Pos / 2) + (width / 2)
                );
                FillRectangle(
                    Bmp,
                    color,
                    0,
                    (HEIGHT / 2) + (Pos / 2) - (width / 2),
                    WIDTH,
                    (HEIGHT / 2) + (Pos / 2) + (width / 2)
                );
            }
            else
                FillRectangle(
                        Bmp,
                        color,
                        0,
                        Pos - (width / 2),
                        WIDTH,
                        Pos + (width / 2)
                    );
            if (texture) Texture();
            return Bmp;
        }
    }
}
