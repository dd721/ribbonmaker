﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RibbonMaker.Draw
{
    class BendFortyFive : RibbonElement
    {
        public BendFortyFive() : base("Diagonal (45 degrees)") { }

        public override RibbonElement GetInstance()
        {
            return new BendFortyFive();
        }

        public override Image Draw(bool texture)
        {
            Clear();
            if (XMirror ^ YMirror)
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 15 - (width / 2) + (WIDTH / 2) + (Pos / 2) + i)), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 15 - (width / 2) + (WIDTH / 2) - (Pos / 2) + i)), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j - 18 + Pos + i)), i, color);
                    }
                }
            }
            else
            {
                if (symmetry)
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 15 - (width / 2) + (WIDTH / 2) + (Pos / 2) - i)), i, color);
                    }
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, j + 15 - (width / 2) + (WIDTH / 2) - (Pos / 2) - i)), i, color);
                    }
                }
                else
                {
                    for (int i = 0; i < HEIGHT; i++)
                    {
                        for (int j = 0; j < width; j++)
                            Bmp.SetPixel(Math.Max(0, Math.Min(WIDTH - 1, 12 + j + Pos - i)), i, color);
                    }
                }
            }
            if (texture) Texture();
            return Bmp;
        }
    }
}
