﻿using System;
using System.Drawing;

namespace RibbonMaker.Draw
{
    public class GraphicElement : RibbonElement
    {
        protected Bitmap baseGraphic;
        public GraphicElement(string description, Bitmap graphic, bool enableTexture=true) : base (description, false, enableTexture)
        {
            this.baseGraphic = graphic;
        }

        public override RibbonElement GetInstance()
        {
            return new GraphicElement(description, baseGraphic, enableTexture);
        }

        public override Image Draw(bool texture)
        {
            Clear();
            Bitmap graphic = new Bitmap(baseGraphic);
            graphic.MakeTransparent();

            for (int i = 0; i < graphic.Width; i++)
                for (int j = 0; j < graphic.Height; j++)
                {
                    Color new_color = graphic.GetPixel(i, j);
                    if (new_color.A != 0)
                    {
                        new_color = Color.FromArgb(new_color.A,
                                Math.Max(0, color.R - (255 - new_color.R)),
                                Math.Max(0, color.G - (255 - new_color.R)),
                                Math.Max(0, color.B - (255 - new_color.R)));

                    }
                        //graphic.SetPixel(i, j, new Color(0, 0, 0));
                        graphic.SetPixel(i, j, new_color);
                }

            int x = graphic.Width;
            int y = graphic.Height;
            using (Graphics g = Graphics.FromImage(Bmp))
            {
                if (symmetry)
                {
                    g.DrawImage(graphic, new Rectangle((Bmp.Width / 2) - (Pos / 2) - (x / 2), 0, x, y), new Rectangle(0, 0, x, y), GraphicsUnit.Pixel);
                    g.DrawImage(graphic, new Rectangle((Bmp.Width / 2) + (Pos / 2) - (x / 2), 0, x, y), new Rectangle(0, 0, x, y), GraphicsUnit.Pixel);
                }
                else
                g.DrawImage(graphic, new Rectangle(Pos - (x / 2), 0, x, y), new Rectangle(0, 0, x, y), GraphicsUnit.Pixel);
            }
            if (texture) Texture();
            return Bmp;
        }       
    }
}
