﻿using System;
using System.Drawing;

namespace RibbonMaker.Draw
{    public class VerticalBar : RibbonElement
    {
        public VerticalBar() : base("Vertical Bar") { }

        public override RibbonElement GetInstance()
        {
            return new VerticalBar();
        }

        public override Image Draw(bool texture)
        {
            Clear();

            if (symmetry)
            {
                FillRectangle(
                    Bmp,
                    color,
                    (WIDTH / 2) - (Pos / 2) - (width / 2),
                    0,
                    (WIDTH / 2) - (Pos / 2) + (width / 2),
                    HEIGHT
                );
                FillRectangle(
                    Bmp,
                    color,
                    (WIDTH / 2) + (Pos / 2) - (width / 2),
                    0,
                    (WIDTH / 2) + (Pos / 2) + (width / 2),
                    HEIGHT
                );
            }
            else
                for (int i = 0; i < HEIGHT; i++)
                    FillRectangle(Bmp, color, Pos - (width / 2), 0, Pos + (width / 2), HEIGHT);
            if (texture) Texture();
            return Bmp;
        }
    }
}
