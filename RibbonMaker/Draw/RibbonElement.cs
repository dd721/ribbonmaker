﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Resources;
using System.Drawing.Imaging;

namespace RibbonMaker.Draw
{
    public class RibbonElement
    {
        public const int WIDTH = 100;
        public const int HEIGHT = 30;
        protected string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public bool symmetry { get; set; }
        public bool texture { get; set; }

        public int pos;
        public int Pos 
        { 
            get
            {
                if (!Center) return pos;
                else return WIDTH / 2;
            }
            set { pos = value; }
        }
        public Color color { get; set; }
        public Bitmap Bmp { get; set; }
        public int width { get; set; }
        public bool XMirror { get; set; }
        public bool YMirror { get; set; }

        public bool Center { get; set; }

        public bool enableWidth { get; }
        public bool enableTexture { get; }

        public override string ToString()
        {
            return description;
        }

        public RibbonElement(RibbonElement e)
        {
            description = e.description;
            symmetry = e.symmetry;
            texture = e.texture;
            pos    = e.pos;
            color = e.color;  
            Bmp = e.Bmp;
            width = e.width;
            XMirror = e.XMirror;
            YMirror = e.YMirror;
            enableWidth = e.enableWidth;
            enableTexture = e.enableTexture;
            Center = e.Center;
        }

        public RibbonElement(string description, bool enableWidth = true, bool enableTexture = true)
        {
            this.description = description;
            this.enableWidth = enableWidth;
            this.enableTexture = enableTexture;
            width = 0;
            symmetry = true;
            pos = 0;
            Center = false;
            Bmp = new Bitmap(WIDTH, HEIGHT, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            Color t = Color.FromArgb(0, 0, 0, 0);
            for (int i = 0; i < WIDTH; i++)
            {
                for (int j = 0; j < HEIGHT; j++)
                    Bmp.SetPixel(i, j, t);
            }
        }

        public virtual RibbonElement GetInstance()
        {
            return new RibbonElement(description);
        }

        public virtual Image Draw(bool texture)
        {
            return Bmp;
        }
        protected void FillRectangle(Bitmap bmp, Color color, int x0, int y0, int x1, int y1)
        {
            for (int i = Math.Max(0, Math.Min(x0, x1)); i < Math.Min(WIDTH, Math.Max(x0, x1)); i++)
            {
                for (int j = Math.Max(0, Math.Min(y0, y1)); j < Math.Min(HEIGHT, Math.Max(y0, y1)); j++)
                {
                    bmp.SetPixel(i, j, color);
                }
            }
        }

        protected void Clear()
        {
            Bmp = new Bitmap(WIDTH, HEIGHT, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
        }

        protected void Texture()
        {
            if (texture && enableTexture)
            {
                const int ADJUST = 30;
                Color color;
                for (int i = 0; i < WIDTH; i++)
                {
                    for (int j = 0; j < HEIGHT; j += 2)
                    {
                        color = Bmp.GetPixel(i, j);
                        if (color.A != 0)
                        {
                            color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                            Bmp.SetPixel(i, j, color);

                            if (color.R == 0 && color.G == 0 && color.B == 0)
                            {
                                color = Bmp.GetPixel(i, j + 1);
                                if (color.A != 0)
                                    color = Color.FromArgb(Math.Min(255, color.R + ADJUST), Math.Min(255, color.G + ADJUST), Math.Min(255, color.B + ADJUST));
                                Bmp.SetPixel(i, j + 1, color);
                            }
                        }
                    }
                }
            }
        }
    }
}
