﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.Windows.Forms;
using RibbonMaker.Draw;
using RibbonMaker.Model;
using System.Linq;
using System.Windows.Media.Media3D;

namespace RibbonMaker.ViewModel
{
    class MedalEditViewModel : ViewModelBase
    {
        public MedalEditViewModel(List<RibbonElement> customElements)
        {
            AddRaisedDetail = false;
            ribbon = new Model.Ribbon(true);
            wipRibbon = new Model.Ribbon(true);
            ribbon.BGColor = Color.Red;
            ElementList = new List<RibbonElement>
            {
                new VerticalBar(),
                new HorizontalBar(),
                new BendFifteen(),
                new BendThirty(),
                new BendFortyFive()
            };
            foreach (RibbonElement element in customElements) { ElementList.Add(element); }
            wipElement = ElementList[0];
            Refresh();
        }

        public int[] CustomColors 
        {  
            get
            {
                return CustomColorSerializer.StringToColors(Properties.Settings.Default.CustomColors);
            }
            set
            {
                Properties.Settings.Default.CustomColors = CustomColorSerializer.ColorsToString(value);
                Properties.Settings.Default.Save();
            }
        }

        public string Version { get { return "RibbonMaker " + Properties.Settings.Default.Version; } }

        public Model.Ribbon ribbon { get; set; }
        public Model.Ribbon wipRibbon { get; set; }
        public List<RibbonElement> ElementList { get; set; }

        public Color cbColor {  get; set; }

        public bool ColorCopied
        {
            get { return cbColor != Color.Empty; }
        }

        private bool addRaisedDetail;
        public bool AddRaisedDetail
        {
            get { return addRaisedDetail; }
            set { addRaisedDetail = value; RaisePropertyChangedEvent("ElementPreview"); }
        }
        public List<RibbonElement> RibbonElements
        {
            get { return ribbon.Elements; }
            set { ribbon.Elements = value; }    
        }

        private RibbonElement wipElement;
        public RibbonElement WipElement { get { return wipElement; } 
            set 
            { 
                wipElement = value;
                if (wipElement.ToString() == "Horizontal Bar")
                {
                    Width = Math.Min(Width, MaxWidth);
                    Pos = Math.Min(Pos, MaxPos);
                }
                RaisePropertyChangedEvent("Width");
                RaisePropertyChangedEvent("WidthEnabled");
                RaisePropertyChangedEvent("MaxPos");
                RaisePropertyChangedEvent("MaxWidth");
                RaisePropertyChangedEvent("ElementPreview");
                Refresh(); 
            } 
        }

        private RibbonElement element;
        public RibbonElement Element
        {
            get { return element; }
            set 
            { 
                element = value;
                RaisePropertyChangedEvent("Element");
                RaisePropertyChangedEvent("Symmetry");
                RaisePropertyChangedEvent("Width");
                RaisePropertyChangedEvent("Pos");
                RaisePropertyChangedEvent("MaxPos");
                RaisePropertyChangedEvent("MaxWidth");
                RaisePropertyChangedEvent("WidthEnabled");
                RaisePropertyChangedEvent("PosEnabled");
                RaisePropertyChangedEvent("RaisedDetail");
                RaisePropertyChangedEvent("XFlip");
                RaisePropertyChangedEvent("YFlip");
                RaisePropertyChangedEvent("CenterElement");
                RaisePropertyChangedEvent("ElementSelected");
            }
        }

        public Ribbon Ribbon
        {
            get { return ribbon; }
        }

        public Bitmap ElementPreview
        {
            get
            {
                Bitmap bmp = new Bitmap(RibbonElement.WIDTH, RibbonElement.HEIGHT, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                if (wipElement != null)
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        RibbonElement e = wipElement.GetInstance();
                        e.texture = !AddRaisedDetail;
                        e.symmetry = false;
                        e.pos = e.ToString() == "Horizontal Bar" ? 15 : 50;
                        e.width = 5;
                        e.color = Color.White;
                        
                        g.Clear(Color.Black);
                        g.DrawImage(e.Draw(false), new Rectangle(0, 0, bmp.Width, bmp.Height));
                    }
                else
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.Clear(Color.DarkGray);
                    }
                return bmp; 
            }
        }

        public int MaxWidth
        {
            get
            {
                int width = 50;
                if (element != null)
                    width = element.ToString() == "Horizontal Bar" ? 25 : 50;
                return width;
            }
        }
        public int MaxPos
        {
            get
            {
                int pos = 99;
                if (element != null)
                    pos = element.ToString() == "Horizontal Bar" ? RibbonElement.HEIGHT : RibbonElement.WIDTH;
                pos -= 1;
                return pos;
            }
        }
        public bool CenterElement
        {
            get 
            { 
                if (element != null)
                    return Element.Center;
                return false;
            }
            set
            {
                if (element != null)
                    Element.Center = value;
                RaisePropertyChangedEvent("Pos");
                RaisePropertyChangedEvent("PosEnabled");
                Refresh();
            }
        }

        public bool ElementSelected
        {
            get
            {
                if (Element == null) return false;
                else return true;
            }
        }

        public bool Symmetry
        {
            get 
            { 
                if (Element != null)
                    return Element.symmetry;
                return false;
            }
            set
            {
                if (Element != null)
                    Element.symmetry = value;
                RaisePropertyChangedEvent("MaxPos");
                RaisePropertyChangedEvent("Pos");
                Refresh();
            }
        }
        public bool RaisedDetail
        {
            get 
            { 
                if (Element != null)    
                    return !Element.texture;
                return false;
            }
            set
            {
                if (Element != null)    
                    Element.texture = !value;
                Refresh();
            }
        }
        public bool XFlip
        {
            get
            {
                if (Element != null)
                    return Element.XMirror;
                return false;
            }
            set
            {
                if (Element != null)
                    Element.XMirror = value;
                Refresh();
            }
        }

        public bool YFlip
        {
            get 
            {
                if (Element != null)
                    return Element.YMirror;
                return false; 
            }
            set
            {
                if (Element != null)
                    Element.YMirror = value;
                Refresh();
            }
        }

        public bool DrawTexture
        {
            get { return ribbon.Texture; }
            set
            {
                ribbon.Texture = value;
                Refresh();
            }
        }

        public int Pos
        {
            get 
            { 
                if (Element != null) return Element.pos;
                return 0;
            }
            set 
            { 
                if (Element != null) Element.pos = value;
                RaisePropertyChangedEvent("Pos");
                Refresh();
            }
        }
        public int Width
        {
            get 
            {
                if (Element != null) return Element.width;
                return 0;
            }
            set
            {
                if (Element != null) Element.width = value;
                RaisePropertyChangedEvent("Width");
                Refresh();
            }
        }

        public bool WidthEnabled
        {
            get
            {
                if (Element != null) return Element.enableWidth;
                return false;
            }
        }

        public bool PosEnabled
        {
            get
            {
                if (Element == null) return false;
                return !Element.Center;
            }
        }


        private ICommand _save_medal;
        private ICommand _clear_medal;
        private ICommand _pick_bg_color;
        private ICommand _pick_el_color;
        private ICommand _delete;
        private ICommand _add_element;
        private ICommand _copy;
        private ICommand _up;
        private ICommand _down;
        private ICommand _pastecolor;
        private ICommand _copycolor;

        private void ClearMedal()
        {
            Color bgColor = ribbon.BGColor;
            bool texture = ribbon.Texture;
            ribbon = new Ribbon
            {
                BGColor = bgColor
            };
            ribbon.Texture = texture;
            ribbon.Draw();
            RaisePropertyChangedEvent("RibbonElements");
            Refresh();
        }

        private void PickBGColor()
        {
            ColorDialog colorPicker = new ColorDialog();
            colorPicker.CustomColors = CustomColors;
            colorPicker.Color = ribbon.BGColor;
            if (colorPicker.ShowDialog() == DialogResult.OK)
            {
                ribbon.BGColor = colorPicker.Color;
                CustomColors = colorPicker.CustomColors;
            }
            Refresh();
        }
        private void PickElementColor()
        {
            ColorDialog colorPicker = new ColorDialog();
            colorPicker.CustomColors = CustomColors;
            if (Element != null) colorPicker.Color = Element.color;
            if (colorPicker.ShowDialog() == DialogResult.OK && Element != null)
            {
                Element.color = colorPicker.Color;
                CustomColors = colorPicker.CustomColors;
            }
            Refresh();
        }

        private void Refresh()
        {
            if (ribbon != null)
            {
                ribbon.Draw();
                RaisePropertyChangedEvent("ribbon");
            }
        }

        private void AddElement()
        {
            if (wipElement != null)
            {
                Type eType = wipElement.GetType();
                CenterElement = false;
                RibbonElement newElement = wipElement.GetInstance();
                newElement.width = 5;
                newElement.pos = wipElement.ToString() == "Horizontal Bar" ? 15 : 50;
                newElement.symmetry = false;
                newElement.texture = !AddRaisedDetail;
                newElement.color = Color.White;
                newElement.XMirror = false;
                newElement.YMirror = false;
                ribbon.Elements.Insert(0, newElement);
                Element = newElement;
                ribbon.Elements = new List<RibbonElement>(ribbon.Elements);
                RaisePropertyChangedEvent("RibbonElements");
                RaisePropertyChangedEvent("Element");
                Refresh();
            }
        }

        private void DeleteElement()
        {
            if (Element != null)
            {
                int index = ribbon.Elements.IndexOf(Element);
                ribbon.Elements.Remove(Element);
                if (ribbon.Elements.Count > 0)
                    Element = ribbon.Elements[Math.Min(ribbon.Elements.Count - 1, index)];
            }
            ribbon.Elements = new List<RibbonElement>(ribbon.Elements);
            RaisePropertyChangedEvent("RibbonElements");
            RaisePropertyChangedEvent("ElementSelected");
            RaisePropertyChangedEvent("Element");
            Refresh();
        }

        private void MoveUp()
        {
            if (Element != null)
            {
                RibbonElement e = Element;
                int index = ribbon.Elements.IndexOf(e);
                ribbon.Elements.Remove(e);
                ribbon.Elements.Insert(Math.Max(0, index - 1), e);
                ribbon.Elements = new List<RibbonElement>(ribbon.Elements);
                RaisePropertyChangedEvent("RibbonElements");
                Refresh();
            }
        }

        private void MoveDown() 
        {
            if (Element != null)
            {
                RibbonElement e = Element;
                int index = ribbon.Elements.IndexOf(e);
                ribbon.Elements.Remove(e);
                ribbon.Elements.Insert(Math.Min(ribbon.Elements.Count, index + 1), e);
                ribbon.Elements = new List<RibbonElement>(ribbon.Elements);
                RaisePropertyChangedEvent("RibbonElements");
                Refresh();
            }
        }

        private void Copy()
        {
            if (Element != null)
            {
                RibbonElement newElement = Element.GetInstance();
                newElement.width = Element.width;
                newElement.pos = Element.pos;
                newElement.symmetry = Element.symmetry;
                newElement.texture = Element.texture;
                newElement.color = Element.color;
                newElement.XMirror = Element.XMirror;
                newElement.YMirror = Element.YMirror;
                ribbon.Elements.Insert(0, newElement);
                Element = newElement;
                ribbon.Elements = new List<RibbonElement>(ribbon.Elements);
            }
            RaisePropertyChangedEvent("RibbonElements");
            RaisePropertyChangedEvent("Element");
            Refresh();
        }

        private void Save()
        {
            System.IO.Stream outStream;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "png files (*.png)|*.png";
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((outStream = saveFileDialog.OpenFile()) != null)
                {
                    ribbon.Save(outStream);
                    outStream.Close();
                }
            }
        }

        private void CopyColor()
        {
            if (Element != null) cbColor = Element.color;
            RaisePropertyChangedEvent("ColorCopied");
        }

        private void PasteColor()
        {
            if (Element != null && cbColor != null) Element.color = cbColor;
            Refresh();
        }

        public ICommand ClearCmd
        {
            get
            {
                if (_clear_medal == null) _clear_medal = new RelayCommand(ClearMedal);
                return _clear_medal;
            }
        }

        public ICommand PickBGColorCmd
        {
            get
            {
                if (_pick_bg_color == null) _pick_bg_color = new RelayCommand(PickBGColor);
                return _pick_bg_color;
            }
        }
        public ICommand PickElementColorCmd
        {
            get
            {
                if (_pick_el_color == null) _pick_el_color = new RelayCommand(PickElementColor);
                return _pick_el_color;
            }
        }
        public ICommand AddElementCmd
        {
            get
            {
                if (_add_element == null) _add_element = new RelayCommand(AddElement);
                return _add_element;
            }
        }

        public ICommand DeleteCmd
        {
            get
            {
                if (_delete == null) _delete = new RelayCommand(DeleteElement);
                return _delete;
            }
        }

        public ICommand SaveCmd
        {
            get
            {
                if (_save_medal == null) _save_medal = new RelayCommand(Save);
                return _save_medal;
            }
        }

        public ICommand CopyCmd
        {
            get
            {
                if (_copy == null) _copy = new RelayCommand(Copy);
                return _copy;
            }
        }
        public ICommand UpCmd
        {
            get
            {
                if (_up == null) _up = new RelayCommand(MoveUp);
                return _up;
            }
        }
        public ICommand DownCmd
        {
            get
            {
                if (_down == null) _down = new RelayCommand(MoveDown);
                return _down;
            }
        }
        public ICommand PasteColorCmd
        {
            get
            {
                if (_pastecolor == null) _pastecolor = new RelayCommand(PasteColor);
                return _pastecolor;
            }
        }
        public ICommand CopyColorCmd
        {
            get
            {
                if (_copycolor == null) _copycolor = new RelayCommand(CopyColor);
                return _copycolor;
            }
        }
    }
}
