﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RibbonMaker.ViewModel
{
    class RelayCommand : ICommand
    {
        private readonly Action _Action;
        private readonly Func<bool> _CanExecute;

        public RelayCommand(Action action, Func<bool> can_execute)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            _Action = action;
            _CanExecute = can_execute;
        }

        public RelayCommand(Action action) : this(action, null)
        {
        }

        public void Execute(object parameter)
        {
            _Action();
        }

        public bool CanExecute(object parameter)
        {
            return _CanExecute == null ? true : _CanExecute();
        }

#pragma warning disable 67
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
#pragma warning restore 67
    }
}
