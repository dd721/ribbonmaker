﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using RibbonMaker.Draw;

namespace RibbonMaker.Model

{
    public class Ribbon
    {
        protected List<RibbonElement> elements;
        private const int WIDTH = 100;
        private const int HEIGHT = 30;
        public bool Texture {  get; set; }
        public Bitmap Bmp { get; }
        public Color BGColor { get; set; }
        public Color BGColor2 { get; set; }

        public List<RibbonElement> Elements
        {
            get { return elements; }
            set { elements = value; }
        }
        public Ribbon(bool texture = false)
        {
            elements = new List<RibbonElement>();
            Texture = texture;
            BGColor = Color.White;
            BGColor2 = BGColor;
            Bmp = new Bitmap(WIDTH, HEIGHT, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            Draw();
        }

        public void Save(System.IO.Stream stream)
        {
            Bmp.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
        }

        public void Undo()
        {
            if (elements.Count != 0)
                elements.Remove(elements.Last());
        }
                       
        public void Draw()
        {
            using (Graphics g = Graphics.FromImage(Bmp))
            {
                g.Clear(BGColor);
                if (Texture) DrawTexture();
                for (int i = elements.Count - 1; i >= 0; i--)
                {
                    RibbonElement e = elements[i];
                    g.DrawImage(e.Draw(Texture), new Rectangle(0, 0, Bmp.Width, Bmp.Height));
                }
            }
            DrawEdge();
        }

        public void FillRectangle(Color color, int x0, int y0, int x1, int y1)
        {
            for (int i = Math.Max(0, Math.Min(x0, x1)); i < Math.Min(WIDTH, Math.Max(x0, x1)); i++)
            {
                for (int j = Math.Max(0, Math.Min(y0, y1)); j < Math.Min(HEIGHT, Math.Max(y0, y1)); j++)
                {
                    Bmp.SetPixel(i, j, color);
                }
            }
        }

        private void DrawTexture()
        {
            const int ADJUST = 30;
            Color color;
            for (int i = 0; i < WIDTH; i++)
            {
                for (int j = 0; j < HEIGHT; j += 2)
                {
                    color = Bmp.GetPixel(i, j);
                    color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                    Bmp.SetPixel(i, j, color);

                    if (color.R == 0 && color.G == 0 && color.B == 0)
                    {
                        color = Bmp.GetPixel(i, j + 1);
                        color = Color.FromArgb(Math.Min(255, color.R + ADJUST), Math.Min(255, color.G + ADJUST), Math.Min(255, color.B + ADJUST));
                         Bmp.SetPixel(i, j + 1, color);
                    }
                }
            }
        }

        private void DrawEdge()
        {
            const int ADJUST = 50;
            Color color;
            for (int i = 0; i < WIDTH; i++)
            {
                color = Bmp.GetPixel(i, 0);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(i, 0, color);
                color = Bmp.GetPixel(i, 1);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(i, 1, color);
                color = Bmp.GetPixel(i, HEIGHT - 1);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(i, HEIGHT - 1, color);
                color = Bmp.GetPixel(i, HEIGHT - 2);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(i, HEIGHT - 2, color);
            }
            for (int i = 2; i < HEIGHT - 2; i++)
            {
                color = Bmp.GetPixel(0, i);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(0, i, color);
                color = Bmp.GetPixel(1, i);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(1, i, color);
                color = Bmp.GetPixel(WIDTH - 1, i);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(WIDTH - 1, i, color);
                color = Bmp.GetPixel(WIDTH - 2, i);
                color = Color.FromArgb(Math.Max(0, color.R - ADJUST), Math.Max(0, color.G - ADJUST), Math.Max(0, color.B - ADJUST));
                Bmp.SetPixel(WIDTH - 2, i, color);
            }
        }
    }
}
