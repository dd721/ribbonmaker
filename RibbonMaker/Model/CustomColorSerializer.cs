﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RibbonMaker.Model
{
    public static class CustomColorSerializer
    {
        public static string ColorsToString(int[] colors)
        {
            string ret = "";
            foreach (int color in colors)
            {
                ret += color.ToString() + ';';
            }
            return ret;
        }

        public static int[] StringToColors(string str)
        {
            string[] sub = str.Split(';');
            int[] colors = new int[16];
            for (int i = 0; i < 16; i++) 
            {
                try
                {
                    colors[i] = int.Parse(sub[i]);
                }
                catch
                {
                    colors[i] = 0xFFFFFF;
                }
            }
            return colors;
        }
    }
}
