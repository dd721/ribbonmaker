﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace RibbonMaker.Converters
{
    //converter class to convert system.drawing.bitmap to wpf useable format
    public class DeviceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            using (MemoryStream memory = new MemoryStream())
            {
                //Bitmap bmp = (Bitmap)value;

                Bitmap bmp = new Bitmap(100, 30);
                using (Graphics gfx = Graphics.FromImage(bmp))
                using (SolidBrush brush = new SolidBrush(Color.Red))
                {
                    gfx.FillRectangle(brush, 0, 0, 100, 30);
                }

                bmp.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage ret = new BitmapImage();
                ret.BeginInit();
                ret.StreamSource = memory;
                ret.CacheOption = BitmapCacheOption.OnLoad;
                ret.EndInit();
                return ret;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
