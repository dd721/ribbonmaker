﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using RibbonMaker.Draw;
using RibbonMaker.Model;
using RibbonMaker.View;

namespace RibbonMaker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected List<RibbonElement> LoadCustomElements()
        {
            List<RibbonElement> ret = new List<RibbonElement>();
            string currentPath = Directory.GetCurrentDirectory();
            DirectoryInfo dInfo = new DirectoryInfo(currentPath + "\\devices");
            if (dInfo.Exists)
            {
                foreach (var dir in dInfo.GetDirectories())
                {
                    foreach (var fi in dir.GetFiles())
                    {
                        string ext = fi.Extension.ToUpper();
                        if (ext == ".PNG")
                        {
                            string filename = dir.Name + "/" + fi.Name;
                            Bitmap bmp = new Bitmap(fi.FullName);
                            if (bmp.Width <= RibbonElement.WIDTH && bmp.Height <= RibbonElement.HEIGHT)
                                ret.Add(new CustomElement(filename, bmp));
                        }
                    }
                }

                foreach (var fi in dInfo.GetFiles())
                {
                    string ext = fi.Extension.ToUpper();
                    if (ext == ".PNG")
                    {
                        string filename = fi.Name;
                        Bitmap bmp = new Bitmap(fi.FullName);
                        if (bmp.Width <= RibbonElement.WIDTH && bmp.Height <= RibbonElement.HEIGHT)
                            ret.Add(new CustomElement(filename, bmp));
                    }
                }
            }
            return ret;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MedalEdit window = new MedalEdit();
            ViewModel.MedalEditViewModel VM = new ViewModel.MedalEditViewModel(LoadCustomElements());
            window.DataContext = VM;
            window.Show();
        }
    }
}