# RibbonMaker

RibbonMaker is a simple utility for creating award ribbons specifically for use in Aurora 4x. 

## Installation

Releases will be [hosted here.](https://drive.google.com/open?id=1kuWf345EhyHkAvNOZtb1BEbHVvRCaAY0)

The program requires the .net environment v4.0 to run.


## Usage

The ribbon is displayed in the top left corner. Click the "Pick Background Color" button to set the base color. Add new graphical elements by selecting from the dropdown box under "Add New Element". Adjust the element's position and width using the position and width sliders. The "Symmetry" check box creates two mirrored elements equally spaced on the ribbon; when checked, the position slider controls the distance between them.

The ribbon with the new element overlaid is displayed in the center of the screen. When satisfied with element size and placing, click the "Add Element" button to add the element to the ribbon. When finished, click the "Save" button to save the ribbon as a 100x30 PNG file.